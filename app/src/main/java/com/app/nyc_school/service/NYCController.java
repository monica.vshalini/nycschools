package com.app.nyc_school.service;

import com.app.nyc_school.model.School;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NYCController {

    static final String BASE_URL = "https://data.cityofnewyork.us/";

    /***
     * Creates object for retrofit
     * @return service class object
     */
    public static ApiInterface getClient() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(ApiInterface.class);
    }

    /****
     * Get all NYC school details
     * @param callBackInterface
     */
    public static void getSchool(final CallBackInterface callBackInterface) {

        ApiInterface apiInterface = NYCController.getClient();
        Call<JsonElement> call = apiInterface.getSchools();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {

                    JsonElement jsonElement = response.body();
                    Type collectionType = new TypeToken<Collection<School>>() {
                    }.getType();
                    List<School> schools = new Gson().fromJson(jsonElement, collectionType);
                    callBackInterface.OnCallBack(schools);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }

    /***
     * Get Specific school detail by dbn
     * @param dbn
     * @param callBackInterface
     */
    public static void getSchoolByDbn(String dbn, final CallBackInterface callBackInterface) {

        ApiInterface apiInterface = NYCController.getClient();
        Call<JsonElement> call = apiInterface.getSchoolById(dbn);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {

                    JsonElement jsonElement = response.body();
                    Type collectionType = new TypeToken<Collection<School>>() {
                    }.getType();
                    List<School> schools = new Gson().fromJson(jsonElement, collectionType);
                    callBackInterface.OnCallBack(schools);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }
}

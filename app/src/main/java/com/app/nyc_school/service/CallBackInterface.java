package com.app.nyc_school.service;


import com.app.nyc_school.model.School;

import java.util.List;

public interface CallBackInterface {

    void OnCallBack(List<School> schools);

}

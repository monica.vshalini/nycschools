package com.app.nyc_school.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.nyc_school.R;
import com.app.nyc_school.model.School;
import com.app.nyc_school.service.CallBackInterface;
import com.app.nyc_school.service.NYCController;

import java.util.List;

/****
 *  Shows the average scores of Math and reading and writing of SAT
 */
public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        final ProgressBar progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String dbn = getIntent().getStringExtra("dbn");
        NYCController.getSchoolByDbn(dbn, new CallBackInterface() {
            @Override
            public void OnCallBack(List<School> schools) {
                School school = schools.get(0);
                TextView schoolNameTv = findViewById(R.id.tv_school_name);
                schoolNameTv.setText(school.getName());
                ((TextView) findViewById(R.id.math_score_name)).setText(school.getMathScore());
                ((TextView) findViewById(R.id.reading_score_name)).setText(school.getReadingScore());
                ((TextView) findViewById(R.id.writing_score_name)).setText(school.getWritingScore());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

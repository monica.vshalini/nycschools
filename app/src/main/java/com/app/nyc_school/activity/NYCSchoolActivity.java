package com.app.nyc_school.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.nyc_school.R;
import com.app.nyc_school.model.School;
import com.app.nyc_school.service.CallBackInterface;
import com.app.nyc_school.service.NYCController;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays all nyc school names
 */
public class NYCSchoolActivity extends AppCompatActivity {

    private SchoolAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nyc_school);

        final ProgressBar progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final RecyclerView schoolListView = findViewById(R.id.view_school_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        schoolListView.setLayoutManager(layoutManager);

        List<School> schools = new ArrayList<>();
        adapter = new SchoolAdapter(this, schools);
        schoolListView.setAdapter(adapter);

        NYCController.getSchool(new CallBackInterface() {
            @Override
            public void OnCallBack(List<School> schools) {
                adapter = new SchoolAdapter(NYCSchoolActivity.this, schools);
                schoolListView.setAdapter(adapter);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    static class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolHolder> {

        private Context mContext;

        private List<School> schoolList;

        SchoolAdapter(Context context, List<School> schools) {
            mContext = context;
            schoolList = schools;
        }

        @NonNull
        @Override
        public SchoolHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.school_item, parent, false);
            return new SchoolHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull SchoolHolder holder, final int position) {

            holder.schoolNameTv.setText(schoolList.get(position).getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ScoreActivity.class);
                    intent.putExtra("dbn", schoolList.get(position).getDbn());
                    mContext.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return schoolList.size();
        }

        public static class SchoolHolder extends RecyclerView.ViewHolder {

            TextView schoolNameTv;

            public SchoolHolder(@NonNull View itemView) {
                super(itemView);
                schoolNameTv = itemView.findViewById(R.id.school_name_tv);
            }
        }
    }

}

package com.app.nyc_school.service;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("resource/f9bf-2cp4.json")
    Call<JsonElement> getSchools();

    @GET("resource/f9bf-2cp4.json")
    Call<JsonElement> getSchoolById(@Query(value = "dbn", encoded = true) String status);

}
